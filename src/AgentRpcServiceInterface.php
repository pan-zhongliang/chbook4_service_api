<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: pzl
 * Date: 2023/10/25
 * Time: 14:40
 * Features:
 */
namespace Chbook4\ServiceApi;

/**
 * 分销商
 */
interface AgentRpcServiceInterface
{
    /**
     * 添加消息通知
     *
     * @param array $params
     *
     * @return array
     */
    public function rpcAddNotice(array $params): array;

    /**
     * 推送模板消息
     *
     * @param array $params
     *
     * @return array
     */
    public function noticeHandle(array $params): array;

}