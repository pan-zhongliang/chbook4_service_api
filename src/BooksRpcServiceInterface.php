<?php
/**
 * Created by PhpStorm.
 * User: dk
 * Date: 2024/2/19
 * Time: 18:29
 * Features:
 */
namespace Chbook4\ServiceApi;

/**
 * 图书库服务
 */
interface BooksRpcServiceInterface
{
    /**
     * 查询ES图书列表
     *
     * @param array $params
     *
     * @return array
     */
    public function getEsList(array $params): array;

    /**
     * 查询图书列表
     *
     * @param array $params
     *
     * @return array
     */
    public function getList(array $params): array;

    /**
     * 获取图书详情
     *
     * @param int $id
     *
     * @return array
     */
    public function getDetail(int $id): array;

    /**
     * 新建图书
     *
     * @param array $params
     *
     * @return array
     */
    public function create(array $params): array;

    /**
     * 更新图书信息
     *
     * @param array $params
     *
     * @return array
     */
    public function update(array $params): array;

    /**
     * 批量更新图书信息
     *
     * @param array $ids
     * @param array $params
     *
     * @return array
     */
    public function batchUpdate(array $ids, array $params): array;

    /**
     * 删除图书信息
     *
     * @param $id
     *
     * @return array
     */
    public function delete($id): array;

    /**
     * 待入库-查询图书列表
     *
     * @param array $params
     *
     * @return array
     */
    public function getWaitList(array $params): array;

    /**
     * 待入库-图书isbn重复列表
     *
     * @param array $params
     *
     * @return array
     */
    public function getWaitListByIsbn(array $params): array;

    /**
     * 待入库-获取图书详情
     *
     * @param int $id
     *
     * @return array
     */
    public function getWaitDetail(int $id): array;

    /**
     * 待入库-新增图书信息
     *
     * @param array $params
     *
     * @return array
     */
    public function createWait(array $params): array;

    /**
     * 待入库-批量新增图书信息
     *
     * @param array $params
     *
     * @return array
     */
    public function batchCreateWait(array $params): array;

    /**
     * 待入库-更新图书信息
     *
     * @param array $params
     *
     * @return array
     */
    public function updateWait(array $params): array;

    /**
     * 待入库-批量更新图书信息
     *
     * @param array $ids
     * @param array $params
     *
     * @return array
     */
    public function batchUpdateWait(array $ids, array $params): array;

    /**
     * 待入库-删除
     *
     * @param $id
     *
     * @return array
     */
    public function deleteWait($id): array;

    /**
     * 待入库-拉取图书信息后处理
     *
     * @param array $params
     *
     * @return array
     */
    public function pullBook(array $params): array;

    /**
     * 待入库-入库
     *
     * @param int $id
     *
     * @return array
     */
    public function join(int $id): array;
}